import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import infoProducts from '../data/data.json';
import SingleProduct from './SingleProduct'
import Navigatiopn from './Navigation';
import Products from './Products';
import Header from './Header';
import About from './About';
import Contact from './Contact';

class Router extends Component{

    state = {
        products : [],
        termSearched : ''
    };

    componentWillMount(){
        this.setState(() => {
            return {products : infoProducts};
        });
    }

    searched = (searchedText) => {
        if(searchedText.length > 3){
            this.setState(() => {
                return {termSearched : searchedText};
            });
        }else{
            this.setState(() => {
                return {termSearched : ''};
            });
        }
    };

    //Component is for static route and render is for dynamic page.
    render(){

        let products = [...this.state.products];
        let termSearched = this.state.termSearched;
        let result;

        if(termSearched !== ''){
            result = products.filter(product => (
                product.nombre.toLowerCase().indexOf(termSearched.toLowerCase()) !== -1
            ))
        }else{
            result = products
        }

        return(
            <BrowserRouter>
                <Switch>
                    <div className="contenedor">
                        <Header/>
                        <Navigatiopn/>

                        <Route exact path={"/"} render={() => (
                            <Products
                                products={result}
                                searched = {this.searched}
                            />
                        )}
                        />
                        <Route exact path={"/about"} component={About} />
                        <Route exact path={"/products"} render={() => (
                            <Products
                                products={result}
                                searched = {this.searched}
                            />
                        )}
                        />
                        <Route exact path={"/product/:productId"} render={(props) => {
                            let idProduct = props.location.pathname.replace('/product/', '');
                            return(
                                <SingleProduct
                                    product = {this.state.products[idProduct]}
                                />
                            )
                        }} />
                        <Route exact path={"/contact"} component={Contact} />
                    </div>
                </Switch>
            </BrowserRouter>
        )
    }
}

export default Router;