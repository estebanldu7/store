import React, { Component } from 'react';
import Product from "./Product";
import SearchBar from "./SearchBar";


class Products extends Component{
    render(){
        return(
            <div className={"productos"}>
                <h2>Nuestros Productos</h2>
                <SearchBar
                    searched = {this.props.searched}
                />
                <ul className={"lista-productos"}>
                    {Object.keys(this.props.products).map(product => (
                        <Product
                            info = {this.props.products[product]}
                            key = {product}
                        />
                    ))}
                </ul>
            </div>

        )
    }
}

export default Products;