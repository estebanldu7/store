import React from 'react';
import '../css/searchBar.css';

class SearchBar extends React.Component{

    readData = (e) => {
        const searchedText = e.target.value;
        this.props.searched(searchedText);
    }

    render(){
        return(
            <form className={"buscador"}>
                <input type={"text"} placeholder={"Buscador"} onChange={this.readData}/>
            </form>
        )
    }
}

export default SearchBar;